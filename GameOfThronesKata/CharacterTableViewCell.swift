//
//  CharacterTableViewCell.swift
//  GameOfThronesKata
//
//  Created by Wayne Delport on 2018/12/04.
//  Copyright © 2018 Glucode. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subheaderLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    public func configure(header: String, subheader: String, title: String, subtitle: String) {
        self.headerLabel.text = header
        self.subheaderLabel.text = subheader
        self.titleLabel.text = title
        self.subtitleLabel.text = subtitle
    }
    
}
